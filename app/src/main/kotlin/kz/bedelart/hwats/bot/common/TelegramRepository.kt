package kz.bedelart.hwats.bot.common

import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.ChatMember
import com.github.kotlintelegrambot.entities.InlineKeyboardMarkup
import com.github.kotlintelegrambot.entities.Message
import com.github.kotlintelegrambot.entities.MessageEntity
import com.github.kotlintelegrambot.entities.ReplyMarkup
import com.github.kotlintelegrambot.entities.User
import com.github.kotlintelegrambot.entities.dice.DiceEmoji
import com.github.kotlintelegrambot.entities.inlinequeryresults.InlineQueryResult
import com.github.kotlintelegrambot.entities.keyboard.InlineKeyboardButton
import com.github.kotlintelegrambot.network.retrofit.converters.ChatIdConverterFactory
import com.github.kotlintelegrambot.network.retrofit.converters.DiceEmojiConverterFactory
import com.github.kotlintelegrambot.network.retrofit.converters.EnumRetrofitConverterFactory
import com.github.kotlintelegrambot.network.serialization.adapter.DiceEmojiAdapter
import com.github.kotlintelegrambot.network.serialization.adapter.InlineKeyboardButtonAdapter
import com.github.kotlintelegrambot.network.serialization.adapter.InlineQueryResultAdapter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import java.util.Timer
import java.util.concurrent.TimeUnit
import kotlin.concurrent.schedule
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

object TelegramRepository {

    private const val BOT_TIMEOUT = 30L
    private const val DELAY_TO_REMOVE_MESSAGE = 300_000L

    private val api: Api by lazy {
        val (botTimeout, timeUnit) = BOT_TIMEOUT to TimeUnit.SECONDS

        val gson = GsonBuilder()
            .registerTypeAdapter(InlineQueryResult::class.java, InlineQueryResultAdapter())
            .registerTypeAdapter(InlineKeyboardButton::class.java, InlineKeyboardButtonAdapter())
            .registerTypeAdapter(DiceEmoji::class.java, DiceEmojiAdapter())
            .create()

        val httpClient = OkHttpClient.Builder()
            .connectTimeout(botTimeout, timeUnit)
            .readTimeout(botTimeout, timeUnit)
            .writeTimeout(botTimeout, timeUnit)
            .enableLogging(true)
            .retryOnConnectionFailure(true)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.telegram.org/bot${Identity.botToken}/")
            .client(httpClient)
            .addConverterFactory(ChatIdConverterFactory())
            .addConverterFactory(EnumRetrofitConverterFactory())
            .addConverterFactory(DiceEmojiConverterFactory())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        retrofit.create(Api::class.java)
    }

    private val gson = Gson()

    fun toJson(source: Any): String = gson.toJson(source)

    fun <T> fromJson(json: String, clazz: Class<T>): T = gson.fromJson(json, clazz)

    suspend fun User.message(text: String) = message(id, text)

    suspend fun message(
        chatId: Long,
        text: String,
        replyToMessageId: Long? = null,
        replyMarkup: ReplyMarkup? = null
    ) = api.message(ChatId.fromId(chatId), text, replyToMessageId, replyMarkup).value

    fun disappearingMessage(
        chatId: Long,
        text: String
    ) {
        val message = runBlocking { message(chatId, text) }
        Timer().schedule(DELAY_TO_REMOVE_MESSAGE) {
            runBlocking { deleteMessage(chatId, message.messageId) }
        }
    }

    suspend fun Message.edit(
        newText: String,
        newEntities: List<MessageEntity>? = entities,
        needToDisablePreview: Boolean = false
    ) = editMessage(chat.id, messageId, newText, replyMarkup, newEntities, needToDisablePreview)

    suspend fun Message.append(
        nextText: String,
        nextEntities: List<MessageEntity>? = null,
        needToDisablePreview: Boolean = false
    ) = editMessage(
        chat.id,
        messageId,
        "${this.text}$nextText",
        replyMarkup,
        entities?.let { old -> nextEntities?.let { old + it } } ?: nextEntities,
        needToDisablePreview
    )

    suspend fun deleteMessage(
        chatId: Long,
        messageId: Long
    ) = api.deleteMessage(ChatId.fromId(chatId), messageId).value

    suspend fun getChatMember(chatId: Long, userId: Long) = api.getChatMember(ChatId.fromId(chatId), userId).value

    private suspend fun editMessage(
        chatId: Long,
        messageId: Long,
        text: String,
        replyMarkup: InlineKeyboardMarkup? = null,
        entities: List<MessageEntity>? = null,
        disablePreview: Boolean = false
    ) = api.editMessageText(
        ChatId.fromId(chatId),
        messageId,
        text = text,
        replyMarkup = replyMarkup,
        entities = entities?.let(TelegramRepository::toJson),
        disableWebPagePreview = disablePreview
    ).value

    private fun OkHttpClient.Builder.enableLogging(value: Boolean): OkHttpClient.Builder {
        takeIf { value } ?: return this
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return addInterceptor(interceptor)
    }

    interface Api {

        @FormUrlEncoded
        @POST("editMessageText")
        suspend fun editMessageText(
            @Field("chat_id") chatId: ChatId?,
            @Field("message_id") messageId: Long?,
            @Field("text") text: String,
            @Field("entities") entities: String?,
            @Field("disable_web_page_preview") disableWebPagePreview: Boolean?,
            @Field("inline_message_id") inlineMessageId: String? = null,
            @Field("reply_markup") replyMarkup: ReplyMarkup? = null,
            @Field("parse_mode") parseMode: String? = null
        ): Result<Message>

        @FormUrlEncoded
        @POST("sendMessage")
        suspend fun message(
            @Field("chat_id") chatId: ChatId,
            @Field("text") text: String,
            @Field("reply_to_message_id") replyToMessageId: Long?,
            @Field("reply_markup") replyMarkup: ReplyMarkup?
        ): Result<Message>

        @FormUrlEncoded
        @POST("deleteMessage")
        suspend fun deleteMessage(
            @Field("chat_id") chatId: ChatId,
            @Field("message_id") messageId: Long
        ): Result<Boolean>

        @GET("getChatMember")
        suspend fun getChatMember(
            @Query("chat_id") chatId: ChatId,
            @Query("user_id") userId: Long
        ): Result<ChatMember>

        data class Result<T>(
            @SerializedName("ok") val isSuccessful: Boolean,
            @SerializedName("result") val value: T,
        )
    }
}
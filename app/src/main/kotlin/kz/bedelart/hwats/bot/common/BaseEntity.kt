package kz.bedelart.hwats.bot.common

interface BaseEntity {
    val id: Long
    val name: String
}
package kz.bedelart.hwats.bot.common

import com.github.kotlintelegrambot.entities.ReplyMarkup

interface TelegramEntity : BaseEntity {

    suspend fun message(
        text: String,
        replyMarkup: ReplyMarkup? = null
    ) = TelegramRepository.message(id, text, replyMarkup = replyMarkup)
}
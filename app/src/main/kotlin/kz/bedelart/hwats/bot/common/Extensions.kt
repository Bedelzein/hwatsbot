package kz.bedelart.hwats.bot.common

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicReference
import kotlinx.coroutines.cancel

@Suppress("unused")
val Any?.unit get() = Unit

fun unit(@Suppress("unused") any: Any?) = Unit

@OptIn(InternalCoroutinesApi::class)
fun <FIRST, SECOND: Any, RESULT> Flow<FIRST>.withLatestFrom(
    other: Flow<SECOND>,
    transform: suspend (FIRST, SECOND) -> RESULT
): Flow<RESULT> = flow {
    coroutineScope {
        val latestSecond = AtomicReference<SECOND?>()
        val outerScope = this
        launch {
            try {
                other.collect(latestSecond::set)
            } catch (e: CancellationException) {
                outerScope.cancel(e) // cancel outer scope on cancellation exception too
            }
        }
        collect { first ->
            latestSecond.get()?.let { second -> emit(transform(first, second)) }
        }
    }
}

suspend fun FlowCollector<Unit>.trigger() = emit(Unit)
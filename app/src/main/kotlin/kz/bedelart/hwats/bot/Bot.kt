package kz.bedelart.hwats.bot

import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.dispatcher.message
import com.github.kotlintelegrambot.entities.InlineKeyboardMarkup
import com.github.kotlintelegrambot.entities.keyboard.InlineKeyboardButton
import kotlinx.coroutines.runBlocking
import kz.bedelart.hwats.bot.common.Hardcode
import kz.bedelart.hwats.bot.common.Identity
import kz.bedelart.hwats.bot.common.TelegramRepository
import kz.bedelzein.core.kotlin.claimNotNull

private const val EMPTY_STRING = ""
private const val REGEX_PATTERN_NOT_NUMBER = "\\D+"
private const val COUNTRY_CODE_KZ = '8'
private const val WHATSAPP_REQUIRED_COUNTRY_CODE_KZ = '7'

fun pollHwatsBot() = bot {
    token = Identity.botToken
    dispatch {
        command("/start") {
            runBlocking {
                TelegramRepository.message(
                    message.chat.id,
                    "To start chatting directly with user in WhatsApp please send phone number"
                )
            }
        }
        message {
            val messageText = claimNotNull(message.text)
            Hardcode.checkIfPhoneIsCorrect(messageText)
            val filteredNumber = messageText
                .replace(Regex(REGEX_PATTERN_NOT_NUMBER), EMPTY_STRING)
                .toCharArray()
                .also { if (it.first() == COUNTRY_CODE_KZ) it[0] = WHATSAPP_REQUIRED_COUNTRY_CODE_KZ }
                .let(::String)

            val link = "https://wa.me/$filteredNumber"
            val linkTyWhatsAppPrivateMessageButton = InlineKeyboardButton.Url(
                "Click to send private message",
                link
            )
            runBlocking {
                TelegramRepository.message(
                    message.chat.id,
                    link,
                    replyMarkup = InlineKeyboardMarkup.create(listOf(listOf(linkTyWhatsAppPrivateMessageButton)))
                )
            }
        }
    }
}.startPolling()
package kz.bedelart.hwats.bot.common

import kz.bedelart.hwats.bot.app.BuildConfig

object Identity {

    private const val CONFIG_KEY_BOT_USERNAME = "BOT_USERNAME"
    private const val CONFIG_KEY_BOT_TOKEN = "BOT_TOKEN"

    val botUsername: String = try {
        System.getenv(CONFIG_KEY_BOT_USERNAME)
    } catch (e: Throwable) {
        Hardcode.handleError(e)
        BuildConfig.CONFIG_KEY_BOT_USERNAME
    }

    val botToken: String = try {
        System.getenv(CONFIG_KEY_BOT_TOKEN)
    } catch (e: Throwable) {
        Hardcode.handleError(e)
        BuildConfig.CONFIG_KEY_BOT_TOKEN
    }
}
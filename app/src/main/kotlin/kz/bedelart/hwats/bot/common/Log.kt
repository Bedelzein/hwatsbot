package kz.bedelart.hwats.bot.common

import org.slf4j.Logger
import org.slf4j.LoggerFactory

private const val LOGGER_EXTENSION_ERROR_MESSAGE = "Unable to get logger from logger class itself"

inline fun <reified T> T.getLogger(): Logger = LoggerFactory.getLogger(T::class.java)

@Deprecated(level = DeprecationLevel.ERROR, message = LOGGER_EXTENSION_ERROR_MESSAGE)
fun Logger.getLogger(): Logger =
    throw IllegalStateException(LOGGER_EXTENSION_ERROR_MESSAGE)

inline fun <reified T> T.logInfo(message: String) = getLogger().info(message)

@Deprecated(level = DeprecationLevel.ERROR, message = LOGGER_EXTENSION_ERROR_MESSAGE)
fun Logger.log(message: String): Unit =
    throw IllegalStateException(LOGGER_EXTENSION_ERROR_MESSAGE)

inline fun <reified T> T.logDebug(message: String) = getLogger().debug(message)

@Deprecated(level = DeprecationLevel.ERROR, message = LOGGER_EXTENSION_ERROR_MESSAGE)
fun Logger.logDebug(message: String): Unit =
    throw IllegalStateException(LOGGER_EXTENSION_ERROR_MESSAGE)

inline fun <reified T> T.logWarning(message: String) = getLogger().warn(message)

@Deprecated(level = DeprecationLevel.ERROR, message = LOGGER_EXTENSION_ERROR_MESSAGE)
fun Logger.logWarning(message: String): Unit =
    throw IllegalStateException(LOGGER_EXTENSION_ERROR_MESSAGE)

inline fun <reified T> T.logError(message: String) = getLogger().error(message)

@Deprecated(level = DeprecationLevel.ERROR, message = LOGGER_EXTENSION_ERROR_MESSAGE)
fun Logger.logError(message: String): Unit =
    throw IllegalStateException(LOGGER_EXTENSION_ERROR_MESSAGE)

inline fun <reified T> T.logTrace(message: String) = getLogger().trace(message)

@Deprecated(level = DeprecationLevel.ERROR, message = LOGGER_EXTENSION_ERROR_MESSAGE)
fun Logger.logTrace(message: String): Unit =
    throw IllegalStateException(LOGGER_EXTENSION_ERROR_MESSAGE)

fun logError(tag: String, message: String) = LoggerFactory.getLogger(tag).error(message)
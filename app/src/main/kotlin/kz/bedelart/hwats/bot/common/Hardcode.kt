package kz.bedelart.hwats.bot.common

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableSharedFlow

@Suppress("MaxLineLength")
@Deprecated("Temporary solution to free developer from blocking feature due to lack of knowledge how to handle this solution properly")
object Hardcode {

    val job = SupervisorJob()

    val scope = CoroutineScope(job)

    @OptIn(ExperimentalCoroutinesApi::class)
    fun clearSharedFlowForNextEmits(flow: MutableSharedFlow<*>) {
        flow.resetReplayCache()
    }

    fun handleError(error: Throwable) {
        logError(error.message ?: error.toString())
    }

    fun checkIfPhoneIsCorrect(messageText: String): Boolean = true
}